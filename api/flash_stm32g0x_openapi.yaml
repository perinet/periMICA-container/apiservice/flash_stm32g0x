---
openapi: 3.0.3
info:
  title: Flash STM32G0X service RESTful API
  version: "1"

tags:
  - name: FlashStm32G0XService
    x-displayName: Redlab Service
    description: | 
      API service for swd flash operations with stm32g0x microcontroller.

paths:
  /flash_stm32g0x:
    get:
      tags:
       - FlashStm32G0XService
      operationId: flash_stm32g0x_get
      summary: Get info object of the service
      responses:
        200:
          description: >
            Info object of the redlab service
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Info'
        500:
          description: >
            Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'

  /flash_stm32g0x/config:
    get:
      tags:
       - FlashStm32G0XService
      operationId: config_get
      summary: Get config object of the service
      responses:
        200:
          description: >
            Config object of the service
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Config'
        500:
          description: >
            Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
    patch:
      tags:
        - FlashStm32G0XService
      operationId: config_patch
      summary: Modify config object
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FLASH_STM32G0X_Config'
      responses:
        204:
          description: No content
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
        500:
          description: Internal Server Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
  /flash_stm32g0x/firmware:
    put:
      tags:
        - FlashStm32G0XService
      operationId: firmware_set
      summary: Put new firmware file onto the target device
      requestBody:
        required: true
        content:
          application/vnd-perinet-fwimage:
            schema:
              $ref: '#/components/schemas/FLASH_STM32G0X_FwImage'
      responses:
        204:
          description: No content
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
        500:
          description: Internal Server Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
  /flash_stm32g0x/option_bytes:
    get:
      tags:
        - FlashStm32G0XService
      operationId: optionbytes_get
      summary: Get current option bytes of the target device
      responses:
        200:
          description: >
            Option bytes value the target device
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_OptionBytes'
        500:
          description: Internal Server Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
    put:
      tags:
       - FlashStm32G0XService
      operationId: optionbytes_set
      summary: Write option bytes of the target device
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FLASH_STM32G0X_OptionBytes'
      responses:
        204:
          description: Option bytes successfully written
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'
        500:
          description: Internal Server Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FLASH_STM32G0X_Error'

components:
  schemas: 
    FLASH_STM32G0X_Info:
      title: FLASH_STM32G0X_Info
      type: object
      properties:
        ApiVersion:
          description: API version
          type: number
          example: 1
          default: 1
        Config:
          $ref: '#/components/schemas/FLASH_STM32G0X_Config'
    FLASH_STM32G0X_Config:
      title: FLASH_STM32G0X_Config
      type: object
      description: Config object of the service
      properties:
        openocd_interface:
          description: Interface descriptor file used by openocd tool
          type: string
          example: /usr/share/openocd/scripts/interface/ftdi/ft232h-module-swd.cfg
          default: /usr/share/openocd/scripts/interface/ftdi/ft232h-module-swd.cfg
        openocd_target:
          description: Target descriptor file used by openocd tool
          type: string
          example: /usr/share/openocd/scripts/target/stm32g0x.cfg
          default: /usr/share/openocd/scripts/target/stm32g0x.cfg
    FLASH_STM32G0X_OptionBytes:
      title: FLASH_STM32G0X_OptionBytes
      type: object
      description: Option bytes of the target device
      properties:
        option_bytes:
          description: >
            64 bit integer representation of the option bytes of the target device
          type: number
          example: 4026531498

    FLASH_STM32G0X_Error:
      title: FLASH_STM32G0X_Error
      type: object
      properties:
        error:
          type: string
          example: ""
      description: Error message of the Flash stm32g0x service.
    FLASH_STM32G0X_FwImage:
      title: FLASH_STM32G0X_FwImage
      description: Firmware image binary stream to be flashed onto the target device
      type: string
      format: binary
