module gitlab.com/perinet/periMICA-container/apiservice/flash_stm32g0x

go 1.21.3

require (
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20230721113448-51dceb287282
	gitlab.com/perinet/generic/lib/utils v0.0.0-20230628130910-2f4300736d64
	gitlab.com/perinet/periMICA-container/app/flash_stm32g0x v0.0.0-20231106114900-1fd291c072b2
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea // indirect
)
