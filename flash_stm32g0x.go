/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package apiservice_flash_stm32g0x

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	filestorage "gitlab.com/perinet/generic/lib/utils/filestorage"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	"gitlab.com/perinet/periMICA-container/app/flash_stm32g0x"
)

type PathInfo = server.PathInfo

type flash_stm32g0x_info struct {
	ApiVersion string                `json:"api_version"`
	Config     flash_stm32g0x_config `json:"config"`
}

type flash_stm32g0x_option_bytes struct {
	OptionBytes int64 `json:"option_bytes"`
}

type flash_stm32g0x_config struct {
	OpenocdInterface string `json:"openocd_interface"`
	OpenocdTarget    string `json:"openocd_target"`
}

const (
	API_VERSION = "1"
	CONFIG_FILE = "/var/lib/apiservice/flash_stm32g0x/config.json"
)

var (
	Logger log.Logger = *log.Default()

	// Default config
	config_cache = flash_stm32g0x_config{
		OpenocdInterface: "/usr/share/openocd/scripts/interface/ftdi/ft232h-module-swd.cfg",
		OpenocdTarget:    "/usr/share/openocd/scripts/target/stm32g0x.cfg",
	}
)

func init() {
	Logger.SetPrefix("ApiServicePeriFLASH: ")
	Logger.Println("Starting")

	// load config
	var tmp flash_stm32g0x_config
	err := filestorage.LoadObject(CONFIG_FILE, &tmp)
	if err != nil {
		Logger.Print(err)
	} else {
		config_cache = tmp
	}

	flash_stm32g0x.Configure(config_cache.OpenocdInterface, config_cache.OpenocdTarget)
}

// URL endpoint definition of the service
func PathsGet() []PathInfo {
	return []PathInfo{
		{Url: "/flash_stm32g0x", Method: server.GET, Role: rbac.NONE, Call: flash_stm32g0x_get},
		{Url: "/flash_stm32g0x/config", Method: server.GET, Role: rbac.NONE, Call: config_get},
		{Url: "/flash_stm32g0x/config", Method: server.PATCH, Role: rbac.NONE, Call: config_set},
		{Url: "/flash_stm32g0x/firmware", Method: server.PUT, Role: rbac.SUPERUSER, Call: firmware_set},
		{Url: "/flash_stm32g0x/option_bytes", Method: server.GET, Role: rbac.USER, Call: optionbytes_get},
		{Url: "/flash_stm32g0x/option_bytes", Method: server.PUT, Role: rbac.USER, Call: optionbytes_set},
	}
}

func flash_stm32g0x_get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	var info = flash_stm32g0x_info{
		ApiVersion: API_VERSION,
		Config:     config_cache,
	}
	res, err := json.Marshal(info)
	if err != nil {
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	webhelper.JsonResponse(w, http_status, res)
}

func config_get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	data, err := json.Marshal(config_cache)
	if err != nil {
		http_status = http.StatusInternalServerError
		data = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	webhelper.JsonResponse(w, http_status, data)
}

func config_set(w http.ResponseWriter, r *http.Request) {
	payload, _ := io.ReadAll(r.Body)
	var tmp_cfg flash_stm32g0x_config
	err := json.Unmarshal(payload, &tmp_cfg)
	if err != nil {
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusBadRequest, data)
		return
	}

	err = filestorage.StoreObject(CONFIG_FILE, tmp_cfg)
	if err != nil {
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}

	webhelper.EmptyResponse(w, http.StatusNoContent)
}

func firmware_set(w http.ResponseWriter, r *http.Request) {
	payload, err := io.ReadAll(r.Body)
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusBadRequest, data)
		return
	}
	f, err := os.CreateTemp("", "")
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}

	_, err = f.Write(payload)
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	f.Close()

	flash_stm32g0x.ResetHalt()
	err = flash_stm32g0x.FlashImage(f.Name())
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	flash_stm32g0x.ResetRun()
	os.Remove(f.Name())

	webhelper.EmptyResponse(w, http.StatusNoContent)
}

func optionbytes_get(w http.ResponseWriter, r *http.Request) {
	res, err := flash_stm32g0x.ReadOptionBytes()
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	var result = flash_stm32g0x_option_bytes{
		OptionBytes: res,
	}
	webhelper.JsonResponse(w, http.StatusOK, result)
}

func optionbytes_set(w http.ResponseWriter, r *http.Request) {
	payload, err := io.ReadAll(r.Body)
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusBadRequest, data)
		return
	}

	var opt_bytes flash_stm32g0x_option_bytes
	err = json.Unmarshal(payload, &opt_bytes)
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusBadRequest, data)
		return
	}

	flash_stm32g0x.ResetHalt()
	err = flash_stm32g0x.UnlockFlashMem()
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	err = flash_stm32g0x.UnlockFlashOpts()
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	err = flash_stm32g0x.WriteOptionBytes(opt_bytes.OptionBytes)
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	var res int64
	res, err = flash_stm32g0x.ReadOptionBytes()
	if err != nil {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	if res != opt_bytes.OptionBytes {
		Logger.Print(err)
		data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		return
	}
	webhelper.EmptyResponse(w, http.StatusNoContent)
}
